﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Geometry
{
    class GeometryBuilder
    {
        public const double Eps = 0.0001f;

        private static bool PointInPolygon(IList<Vertex> polygon, Vertex point)
        {
            var result = false;
            var j = polygon.Count - 1;
            for (var i = 0; i < polygon.Count; i++)
            {
                var vi = polygon[i];
                var vj = polygon[j];
                if ((vi.Y < point.Y && vj.Y >= point.Y
                    || vj.Y < point.Y && vi.Y >= point.Y)
                    && (vi.X <= point.X || vj.X <= point.X))
                {
                    if (vi.X + (point.Y - vi.Y) / (vj.Y - vi.Y) * (vj.X - vi.X) < point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
        
        private bool SegmentCross(Vertex a, Vertex b, Vertex c, Vertex d, out Vertex cross)
        {
            cross = new Vertex();
            var n = new Vertex((d - c).Y, (c - d).X);
            var dot = Vertex.Dot(n, b - a);
            if (dot == 0)
            {
                return false;
            }
            var num = -Vertex.Dot(n, (a - c)) / dot;
            n = (b - a);
            cross.X = a.X + num * n.X;
            cross.Y = a.Y + num * n.Y;
            var classify_ab = Vertex.Classify(a, b, cross);
            var classify_cd = Vertex.Classify(c, d, cross);
            if ((classify_ab == VertexPosition.BETWEEN) && (classify_cd == VertexPosition.BETWEEN))
                return true;
            if ((classify_ab == VertexPosition.BETWEEN) && ((classify_cd == VertexPosition.ORIGIN) || (classify_cd == VertexPosition.DESTINATION)))
                return true;
            if ((classify_cd == VertexPosition.BETWEEN) && ((classify_ab == VertexPosition.ORIGIN) || (classify_ab == VertexPosition.DESTINATION)))
                return true;
            return false;
        }

        private class VertexComparer : IComparer<int>
        {
            private List<Vertex> m_Vertexes;

            public VertexComparer(List<Vertex> vertexes)
            {
                m_Vertexes = vertexes;
            }
            

            public int Compare(int a, int b)
            {
                var x = m_Vertexes[a];
                var y = m_Vertexes[b];
                return CompareVertex(x, y);
            }
        }

        private static int CompareVertex(Vertex x, Vertex y)
        {
            var delta = x.X - y.X;
            if (Math.Abs(delta) < Eps)
            {
                delta = x.Y - y.Y;
                if (Math.Abs(delta) < Eps)
                    return 0;
            }
            return Math.Sign(delta);
        }

        private class EdgeEqualityComparer : IEqualityComparer<Edge>
        {

            public static readonly EdgeEqualityComparer Current = new EdgeEqualityComparer();

            public bool Equals(Edge x, Edge y)
            {
                return ((x.A == y.A) && (x.B == y.B)) || ((x.B == y.A) && (x.A == y.B));
            }

            public int GetHashCode(Edge obj)
            {
                return obj.A ^ obj.B;
            }
        }

        private List<Vertex> m_Vertexes = new List<Vertex>();

        private List<int> m_Indexes = new List<int>();

        private double m_AreaA = 0.0;

        private double m_AreaB = 0.0;

        private double m_AreaOperation = 0.0;

        private VertexComparer m_Comparer;

        public GeometryBuilder()
        {
            m_Comparer = new VertexComparer(m_Vertexes);
        }

        private HashSet<Edge> m_Edges = new HashSet<Edge>(EdgeEqualityComparer.Current);

        private List<Triangle> m_Triangles = new List<Triangle>();

        private int AddVertex(Vertex a)
        {
            var cnt = m_Vertexes.Count;
            m_Vertexes.Add(a);
            var index = m_Indexes.BinarySearch(cnt, m_Comparer);
            if (index < 0)
            {
                index = ~index;
                m_Indexes.Insert(index, cnt);
            }
            else
            {
                m_Vertexes.RemoveAt(cnt);
            }
            return m_Indexes[index];
        }

        private void Cross()
        {
            var edges = new List<Edge>(m_Edges);
            int cnt = 0;
            while (cnt < edges.Count)
            {
                var edge = edges[cnt++];
                for (int i = cnt; i < edges.Count; i++)
                {
                    var check_edge = edges[i];
                    Vertex cross;
                    var v1 = m_Vertexes[edge.A];
                    var v2 = m_Vertexes[edge.B];
                    var v3 = m_Vertexes[check_edge.A];
                    var v4 = m_Vertexes[check_edge.B];
                    if (SegmentCross(v1, v2, v3, v4, out cross))
                    {
                        var index = AddVertex(cross);
                        m_Edges.Remove(edge);
                        m_Edges.Remove(check_edge);

                        var edge1 = new Edge(edge.A, index);
                        var edge2 = new Edge(index, edge.B);
                        var edge3 = new Edge(check_edge.A, index);
                        var edge4 = new Edge(index, check_edge.B);

                        m_Edges.Add(edge1);
                        m_Edges.Add(edge2);
                        m_Edges.Add(edge3);
                        m_Edges.Add(edge4);

                        edges[--cnt] = edge1;
                        edges[i] = edge2;
                        edges.Add(edge3);
                        edges.Add(edge4);
                        break;
                    }
                }
            }
        }

        private void AddEdge(Vertex v1, Vertex v2)
        {
            var a = AddVertex(v1);
            var b = AddVertex(v2);
            if (a != b)
            {
                m_Edges.Add(new Edge(a, b));
            }
        }

        private bool ValidEdge(Vertex a, Vertex b)
        {
            foreach (var edge in m_Edges)
            {
                Vertex c;
                if (SegmentCross(a, b, m_Vertexes[edge.A], m_Vertexes[edge.B], out c))
                {
                    return false;
                }
            }
            return true;
        }

        private void BuildEdges()
        {
            var edges = new List<Edge>();
            for (int i = 0; i < m_Vertexes.Count - 1; i++)
            {
                for (int j = 0; j < m_Vertexes.Count; j++)
                {
                    if (j == i)
                        continue;
                    var edge = new Edge(i, j);
                    if (m_Edges.Contains(edge))
                        continue;
                    edges.Add(edge);
                }
            }
            edges.Sort((e1, e2) =>
            {
                var l1 = (m_Vertexes[e1.A] - m_Vertexes[e1.B]).SquaredLength;
                var l2 = (m_Vertexes[e2.A] - m_Vertexes[e2.B]).SquaredLength;
                return l1.CompareTo(l2);
            });
            for (int i = 0; i < edges.Count; i++)
            {
                var edge = edges[i];
                if (m_Edges.Contains(edge))
                    continue;
                if (ValidEdge(m_Vertexes[edge.A], m_Vertexes[edge.B]))
                    m_Edges.Add(edge);
            }
        }

        private void BuildTriangles(Edge edge, IEnumerable<int> other, IList<Vertex> a, IList<Vertex> b, Operation operation)
        {
            var buffer = new List<Triangle>(4);
            foreach (var index in other)
            {
                buffer.Add(new Triangle() { A = m_Vertexes[edge.A], B = m_Vertexes[edge.B], C = m_Vertexes[index] });
            }
            buffer.Sort((t1, t2) => t1.Area.CompareTo(t2.Area));
            int count = 0;
            while (count < buffer.Count)
            {
                var triangle = buffer[count++];
                var center = triangle.Center;
                for (int j = buffer.Count - 1; j >= count; j--)
                {
                    var big_t = buffer[j];
                    if (big_t.Inside(center))
                        buffer.RemoveAt(j);
                }
                var inside_a = PointInPolygon(a, center);
                var inside_b = PointInPolygon(b, center);
                var area = triangle.Area;
                if (inside_a)
                    m_AreaA += area;
                if (inside_b)
                    m_AreaB += area;
                switch (operation)
                {
                    case Operation.Union:
                        if (inside_a || inside_b)
                        {
                            m_Triangles.Add(triangle);
                            m_AreaOperation += area;
                        }
                        break;
                    case Operation.Cross:
                        if (inside_a && inside_b)
                        {
                            m_Triangles.Add(triangle);
                            m_AreaOperation += area;
                        }
                        break;
                    case Operation.Difference:
                        if (inside_a && (!inside_b))
                        {
                            m_Triangles.Add(triangle);
                            m_AreaOperation += area;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void Build(IList<Vertex> a, IList<Vertex> b, Operation operation)
        {
            var contour1 = a;
            if (contour1.Count > 0)
            {
                if (CompareVertex(contour1[0], contour1[contour1.Count - 1]) != 0)
                {
                    contour1 = new List<Vertex>(contour1);
                    contour1.Add(contour1[0]);
                }
            }
            var contour2 = b;
            if (contour2.Count > 0)
            {
                if (CompareVertex(contour2[0], contour2[contour2.Count - 1]) != 0)
                {
                    contour2 = new List<Vertex>(contour2);
                    contour2.Add(contour2[0]);
                }
            }
            m_Edges.Clear();
            m_Triangles.Clear();
            m_Indexes.Clear();
            m_Vertexes.Clear();
            m_AreaA = 0.0;
            m_AreaB = 0.0;
            m_AreaOperation = 0.0;
            for (int i = 0; i < contour1.Count; i++)
            {
                AddEdge(contour1[i], contour1[(i + 1) % contour1.Count]);
            }
            for (int i = 0; i < contour2.Count; i++)
            {
                AddEdge(contour2[i], contour2[(i + 1) % contour2.Count]);
            }
            Cross();
            BuildEdges();
            var edges = new List<Edge>(Edges);
            for (int i = edges.Count - 1; i >= 0; i--)
            {
                var edge = edges[i];
                if (edge.A == edge.B)
                    edges.RemoveAt(i);
            }
            m_Triangles.Clear();
            for (int i = 0; i < edges.Count; i++)
            {
                var edge1 = edges[i];
                var buffer_a = new HashSet<int>();
                var buffer_b = new HashSet<int>();
                for (int j = 0; j < edges.Count; j++)
                {
                    if (j == i)
                        break;
                    var edge2 = edges[j];
                    if (edge1.A == edge2.A)
                    {
                        buffer_a.Add(edge2.B);
                    }
                    if (edge1.A == edge2.B)
                    {
                        buffer_a.Add(edge2.A);
                    }
                    if (edge1.B == edge2.A)
                    {
                        buffer_b.Add(edge2.B);
                    }
                    if (edge1.B == edge2.B)
                    {
                        buffer_b.Add(edge2.A);
                    }
                }
                buffer_a.IntersectWith(buffer_b);
                BuildTriangles(edge1, buffer_a, contour1, contour2, operation);
            }
        }

        public IList<Vertex> Vertexes
        {
            get
            {
                return m_Vertexes;
            }
        }

        public IEnumerable<Edge> Edges
        {
            get
            {
                return m_Edges;
            }
        }

        public IEnumerable<Triangle> Triangles
        {
            get
            {
                return m_Triangles;
            }
        }

        public double AreaA
        {
            get
            {
                return m_AreaA;
            }
        }

        public double AreaB
        {
            get
            {
                return m_AreaB;
            }
        }

        public double AreaOperation
        {
            get
            {
                return m_AreaOperation;
            }
        }
    }
}
