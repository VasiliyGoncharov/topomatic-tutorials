﻿using PolygonOverlay.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Undo
{
    class UndoCommand
    {
        private bool m_IsA;

        private Vertex[] m_Vertexes;

        public UndoCommand(IList<Vertex> vertexes, bool isA)
        {
            m_Vertexes = vertexes.ToArray();
            m_IsA = isA;
        }

        public bool IsA
        {
            get
            {
                return m_IsA;
            }
        }

        public void Restore(IList<Vertex> vertexes)
        {
            vertexes.Clear();
            for (int i = 0; i < m_Vertexes.Length; i++)
            {
                vertexes.Add(m_Vertexes[i]);
            }
        }
    }
}
