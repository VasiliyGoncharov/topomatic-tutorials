﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Geometry
{
    struct Triangle
    {
        public Vertex A;

        public Vertex B;

        public Vertex C;

        public Vertex Center
        {
            get
            {
                var a_length = (B - C).Length;
                var b_length = (A - C).Length;
                var c_length = (A - B).Length;
                var summ = a_length + b_length + c_length;
                return new Vertex((A.X * a_length + B.X * b_length + C.X * c_length) / summ, 
                    (A.Y * a_length + B.Y * b_length + C.Y * c_length) / summ);
            }
        }

        public double Area
        {
            get
            {
                var ab = (B - A);
                var ac = (C - A);
                return 0.5 * Math.Abs(ab.X * ac.Y - ac.X * ab.Y);
            }
        }

        public bool Inside(Vertex vertex)
        {
            var sign1 = Math.Sign((A.X - vertex.X) * (B.Y - A.Y) - (B.X - A.X) * (A.Y - vertex.Y));
            var sign2 = Math.Sign((B.X - vertex.X) * (C.Y - B.Y) - (C.X - B.X) * (B.Y - vertex.Y));
            var sign3 = Math.Sign((C.X - vertex.X) * (A.Y - C.Y) - (A.X - C.X) * (C.Y - vertex.Y));
            return ((sign1 == sign2) && (sign1 == sign3));
        }
    }
}
