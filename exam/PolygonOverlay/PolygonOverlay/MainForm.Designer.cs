﻿namespace PolygonOverlay
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbLoad = new System.Windows.Forms.ToolStripButton();
            this.tsbSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAddA = new System.Windows.Forms.ToolStripButton();
            this.tsbEditA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAddB = new System.Windows.Forms.ToolStripButton();
            this.tsbEditB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbOverlay = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbUndo = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbCommand = new System.Windows.Forms.ToolStripStatusLabel();
            this.geometryView1 = new PolygonOverlay.Controls.GeometryView();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbLoad,
            this.tsbSave,
            this.toolStripSeparator3,
            this.tsbAddA,
            this.tsbEditA,
            this.toolStripSeparator2,
            this.tsbAddB,
            this.tsbEditB,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.tsbOverlay,
            this.toolStripSeparator4,
            this.tsbUndo});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbLoad
            // 
            this.tsbLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLoad.Image = ((System.Drawing.Image)(resources.GetObject("tsbLoad.Image")));
            this.tsbLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLoad.Name = "tsbLoad";
            this.tsbLoad.Size = new System.Drawing.Size(23, 22);
            this.tsbLoad.Text = "Сохранить";
            this.tsbLoad.Click += new System.EventHandler(this.tsbLoad_Click);
            // 
            // tsbSave
            // 
            this.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
            this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSave.Name = "tsbSave";
            this.tsbSave.Size = new System.Drawing.Size(23, 22);
            this.tsbSave.Text = "Открыть";
            this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAddA
            // 
            this.tsbAddA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddA.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddA.Image")));
            this.tsbAddA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddA.Name = "tsbAddA";
            this.tsbAddA.Size = new System.Drawing.Size(23, 22);
            this.tsbAddA.Text = "Назначить  А";
            this.tsbAddA.Click += new System.EventHandler(this.tsbAddA_Click);
            // 
            // tsbEditA
            // 
            this.tsbEditA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditA.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditA.Image")));
            this.tsbEditA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditA.Name = "tsbEditA";
            this.tsbEditA.Size = new System.Drawing.Size(23, 22);
            this.tsbEditA.Text = "Редактировать А";
            this.tsbEditA.Click += new System.EventHandler(this.tsbEditA_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAddB
            // 
            this.tsbAddB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddB.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddB.Image")));
            this.tsbAddB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddB.Name = "tsbAddB";
            this.tsbAddB.Size = new System.Drawing.Size(23, 22);
            this.tsbAddB.Text = "Назначить В";
            this.tsbAddB.Click += new System.EventHandler(this.tsbAddB_Click);
            // 
            // tsbEditB
            // 
            this.tsbEditB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditB.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditB.Image")));
            this.tsbEditB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditB.Name = "tsbEditB";
            this.tsbEditB.Size = new System.Drawing.Size(23, 22);
            this.tsbEditB.Text = "Редактировать В";
            this.tsbEditB.Click += new System.EventHandler(this.tsbEditB_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(62, 22);
            this.toolStripLabel1.Text = "Операция";
            // 
            // tsbOverlay
            // 
            this.tsbOverlay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsbOverlay.Items.AddRange(new object[] {
            "Пересечение",
            "Объединение",
            "Разность"});
            this.tsbOverlay.Name = "tsbOverlay";
            this.tsbOverlay.Size = new System.Drawing.Size(121, 25);
            this.tsbOverlay.SelectedIndexChanged += new System.EventHandler(this.tsbOverlay_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbUndo
            // 
            this.tsbUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUndo.Enabled = false;
            this.tsbUndo.Image = ((System.Drawing.Image)(resources.GetObject("tsbUndo.Image")));
            this.tsbUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUndo.Name = "tsbUndo";
            this.tsbUndo.Size = new System.Drawing.Size(23, 22);
            this.tsbUndo.Text = "Отменить";
            this.tsbUndo.Click += new System.EventHandler(this.tsbUndo_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbCommand});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbCommand
            // 
            this.lbCommand.Name = "lbCommand";
            this.lbCommand.Size = new System.Drawing.Size(0, 17);
            // 
            // geometryView1
            // 
            this.geometryView1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.geometryView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.geometryView1.Location = new System.Drawing.Point(0, 25);
            this.geometryView1.Name = "geometryView1";
            this.geometryView1.Operation = PolygonOverlay.Geometry.Operation.Union;
            this.geometryView1.Size = new System.Drawing.Size(800, 403);
            this.geometryView1.TabIndex = 0;
            this.geometryView1.Text = "geometryView1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.geometryView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "Операции с многоугольниками";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.GeometryView geometryView1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox tsbOverlay;
        private System.Windows.Forms.ToolStripButton tsbEditA;
        private System.Windows.Forms.ToolStripButton tsbAddA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbAddB;
        private System.Windows.Forms.ToolStripButton tsbEditB;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbLoad;
        private System.Windows.Forms.ToolStripButton tsbSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbUndo;
        private System.Windows.Forms.ToolStripStatusLabel lbCommand;
    }
}