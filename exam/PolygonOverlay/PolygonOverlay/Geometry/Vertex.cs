﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Geometry
{
    //    СЛЕВА, СПРАВА, ВПЕРЕДИ, ПОЗАДИ, МЕЖДУ,   НАЧАЛО, КОНЕЦ
    enum VertexPosition
    {
        LEFT,
        RIGHT,
        BEYOND,
        BEHIND,
        BETWEEN,
        ORIGIN,
        DESTINATION
    };

    struct Vertex
    {
        public double X;

        public double Y;

        public Vertex(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(SquaredLength);
            }
        }

        public double SquaredLength
        {
            get
            {
                return X * X + Y * Y;
            }
        }

        public static Vertex operator -(Vertex a, Vertex b)
        {
            return new Vertex(a.X - b.X, a.Y - b.Y);
        }

        public static Vertex operator +(Vertex a, Vertex b)
        {
            return new Vertex(a.X + b.X, a.Y + b.Y);
        }

        public static VertexPosition Classify(Vertex p1, Vertex p2, Vertex pos)
        {
            var a = p2 - p1;
            var b = pos - p1;
            var sqr_eps = GeometryBuilder.Eps * GeometryBuilder.Eps;
            var sa = a.X * b.Y - b.X * a.Y;
            if (sa > GeometryBuilder.Eps)
                return VertexPosition.LEFT;
            if (sa < -GeometryBuilder.Eps)
                return VertexPosition.RIGHT;
            if ((a.X * b.X < -sqr_eps) || (a.Y * b.Y < -sqr_eps))
                return VertexPosition.BEHIND;
            if ((a.SquaredLength - b.SquaredLength) < -sqr_eps)
                return VertexPosition.BEYOND;
            if ((p1 - pos).SquaredLength < sqr_eps)
                return VertexPosition.ORIGIN;
            if ((p2 - pos).SquaredLength < sqr_eps)
                return VertexPosition.DESTINATION;
            return VertexPosition.BETWEEN;
        }

        public static double Dot(Vertex p, Vertex q)
        {
            return (p.X * q.X + p.Y * q.Y);
        }

    }
}
