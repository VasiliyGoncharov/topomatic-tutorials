﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using Topomatic.ApplicationPlatform;
using Topomatic.ApplicationPlatform.Plugins;
using Topomatic.Cad.Foundation;
using Topomatic.Cad.View;
using Topomatic.Cad.View.Hints;
using Topomatic.Controls.Dialogs;

namespace tutorial5
{
    partial class Module : Topomatic.ApplicationPlatform.Plugins.PluginInitializator
    {
        //Используем статический список для хранения наших данных в качестве модели
        private static List<DataValue> m_Values = new List<DataValue>();

        [cmd("test_dlg_and_table")]
        public void TestDlgAndTable()
        {
            //Просто вызываем метод у нашего диалога
            TestSimpleDialog.Execute(m_Values);
        }
    }
}
