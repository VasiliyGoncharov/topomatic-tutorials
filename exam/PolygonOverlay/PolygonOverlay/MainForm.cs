﻿using PolygonOverlay.Geometry;
using PolygonOverlay.Undo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PolygonOverlay
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var p = geometryView1.A;
            p.Add(new Vertex() { X = 0.0f, Y = 0.0f });
            p.Add(new Vertex() { X = 0.0f, Y = 10.0f });
            p.Add(new Vertex() { X = 8.0f, Y = 10.0f });
            p.Add(new Vertex() { X = 10.0f, Y = 10.0f });
            p.Add(new Vertex() { X = 10.0f, Y = 0.0f });


            p = geometryView1.B;
            p.Add(new Vertex() { X = 5.0f, Y = 5.0f });
            p.Add(new Vertex() { X = 5.0f, Y = 15.0f });
            p.Add(new Vertex() { X = 15.0f, Y = 15.0f });
            p.Add(new Vertex() { X = 15.0f, Y = 5.0f });
            tsbOverlay.SelectedIndex = 0;
        }

        private void tsbOverlay_SelectedIndexChanged(object sender, EventArgs e)
        {
            geometryView1.Operation = (Operation)tsbOverlay.SelectedIndex;
            geometryView1.UpdateView();
            geometryView1.Invalidate();

        }

        private void tsbEditA_Click(object sender, EventArgs e)
        {
            var command = new UndoCommand(geometryView1.A, true);
            if (AddVertexesDlg.Execute(geometryView1.A))
            {
                m_Stack.Push(command);
                geometryView1.UpdateView();
                tsbUndo.Enabled = m_Stack.Count > 0;
            }
        }

        private void tsbAddA_Click(object sender, EventArgs e)
        {
            var command = new UndoCommand(geometryView1.A, true);
            lbCommand.Text = "Укажите положение контура А";
            geometryView1.AppendA(delegate
            {
                m_Stack.Push(command);
                tsbUndo.Enabled = m_Stack.Count > 0;
                lbCommand.Text = string.Empty;
            });
        }

        private void tsbEditB_Click(object sender, EventArgs e)
        {
            var command = new UndoCommand(geometryView1.B, false);
            if (AddVertexesDlg.Execute(geometryView1.B))
            {
                m_Stack.Push(command);
                geometryView1.UpdateView();
                tsbUndo.Enabled = m_Stack.Count > 0;
            }
        }

        private void tsbAddB_Click(object sender, EventArgs e)
        {
            var command = new UndoCommand(geometryView1.B, false);
            lbCommand.Text = "Укажите положение контура B";
            geometryView1.AppendB(delegate
            {
                m_Stack.Push(command);
                tsbUndo.Enabled = m_Stack.Count > 0;
                lbCommand.Text = string.Empty;
            });
        }

        const char COMMENT = '#';

        const char SEPARATOR = ';';

        const char A = 'A';

        const char B = 'B';

        private void tsbLoad_Click(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog())
            {
                dlg.Filter = "CSV|*.csv|Все файлы|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    geometryView1.A.Clear();
                    geometryView1.B.Clear();
                    using (var stream = new FileStream(dlg.FileName, FileMode.Open))
                    {
                        using (var reader = new StreamReader(stream, Encoding.Unicode))
                        {
                            IList<Vertex> vertexes = null;
                            while (!reader.EndOfStream)
                            {
                                var value = reader.ReadLine().Trim();
                                if (value.Length > 0)
                                {
                                    if (value[0] == COMMENT)
                                    {
                                        continue;
                                    }
                                    else if (value[0] == A)
                                    {
                                        vertexes = geometryView1.A;
                                    }
                                    else if (value[0] == B)
                                    {
                                        vertexes = geometryView1.B;
                                    }
                                    else
                                    {
                                        var values = value.Split(SEPARATOR);
                                        Debug.Assert(values.Length == 2);
                                        vertexes.Add(new Vertex(double.Parse(values[0]), double.Parse(values[1])));
                                    }
                                }
                            }
                        }
                    }
                    m_Stack.Clear();
                    geometryView1.UpdateView();
                    tsbUndo.Enabled = m_Stack.Count > 0;
                }
            }
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.OverwritePrompt = true;
                dlg.Filter = "CSV|*.csv|Все файлы|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    using (var stream = new FileStream(dlg.FileName, FileMode.Create))
                    {
                        using (var writer = new StreamWriter(stream, Encoding.Unicode))
                        {
                            writer.WriteLine("#Все строки начинающиеся с # являются комментариями и игнорируются");
                            writer.WriteLine("#Начало контура А");
                            writer.WriteLine(A);
                            for (int i = 0; i < geometryView1.A.Count; i++)
                            {
                                var vertex = geometryView1.A[i];
                                writer.Write(vertex.X);
                                writer.Write(SEPARATOR);
                                writer.WriteLine(vertex.Y);
                            }
                            writer.WriteLine(B);
                            for (int i = 0; i < geometryView1.B.Count; i++)
                            {
                                var vertex = geometryView1.B[i];
                                writer.Write(vertex.X);
                                writer.Write(SEPARATOR);
                                writer.WriteLine(vertex.Y);
                            }
                        }
                    }
                }
            }
        }

        private Stack<UndoCommand> m_Stack = new Stack<UndoCommand>();

        private void tsbUndo_Click(object sender, EventArgs e)
        {
            if (m_Stack.Count > 0)
            {
                var command = m_Stack.Pop();
                if (command.IsA)
                {
                    command.Restore(geometryView1.A);
                }
                else
                {
                    command.Restore(geometryView1.B);
                }
                geometryView1.UpdateView();
                tsbUndo.Enabled = m_Stack.Count > 0;
            }
        }
    }
}
