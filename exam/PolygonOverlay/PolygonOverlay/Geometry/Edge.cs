﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Geometry
{
    struct Edge
    {
        public int A;

        public int B;

        public Edge(int index1, int index2)
        {
            if (index1 < index2)
            {
                A = index1;
                B = index2;
            }
            else
            {
                A = index2;
                B = index1;
            }
        }
    }
}
