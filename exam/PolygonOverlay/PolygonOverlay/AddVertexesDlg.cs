﻿using PolygonOverlay.Geometry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PolygonOverlay
{
    partial class AddVertexesDlg : Form
    {

        private IList<Vertex> m_Vertexes;

        public AddVertexesDlg()
        {
            InitializeComponent();
        }

        private void tsbClear_Click(object sender, EventArgs e)
        {
            dataTable1.BeginLoadData();
            try
            {
                dataTable1.Clear();
            }
            finally
            {
                dataTable1.EndLoadData();
            }
        }

        private void tsbAdd_Click(object sender, EventArgs e)
        {
            dataTable1.BeginLoadData();
            try
            {
                dataTable1.LoadDataRow(new object[] { 0.0, 0.0 }, true);
            }
            finally
            {
                dataTable1.EndLoadData();
            }
        }

        private void tsbRemove_Click(object sender, EventArgs e)
        {
            dataTable1.BeginLoadData();
            try
            {
                var selected = dataGridView1.SelectedCells;
                for (int i = selected.Count - 1; i >= 0; i--)
                {
                    dataTable1.Rows.RemoveAt(selected[i].RowIndex);
                }
            }
            finally
            {
                dataTable1.EndLoadData();
            }
        }

        private void AddVertexesDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dataGridView1.EndEdit())
            {
                if (this.DialogResult == DialogResult.OK)
                {
                    m_Vertexes.Clear();
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        var vertex = new Vertex();
                        var data = dataTable1.Rows[i].ItemArray;
                        vertex.X = (float)((double)data[0]);
                        vertex.Y = (float)((double)data[1]);
                        m_Vertexes.Add(vertex);
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void AddVertexesDlg_Load(object sender, EventArgs e)
        {
            dataTable1.BeginLoadData();
            try
            {
                dataTable1.Rows.Clear();
                for (int i = 0; i < m_Vertexes.Count; i++)
                {
                    var vertex = m_Vertexes[i];
                    dataTable1.LoadDataRow(new object[] { vertex.X, vertex.Y }, i == m_Vertexes.Count - 1);
                }
            }
            finally
            {
                dataTable1.EndLoadData();
            }
        }

        public static bool Execute(IList<Vertex> vertexes)
        {
            using (var dlg = new AddVertexesDlg())
            {
                dlg.m_Vertexes = vertexes;
                return dlg.ShowDialog() == DialogResult.OK;
            }
        }
    }
}
