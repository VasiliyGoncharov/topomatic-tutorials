﻿using PolygonOverlay.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PolygonOverlay.Controls
{
    class GeometryView : Control
    {
        enum EditState
        {
            None,
            A,
            B,
        }

        private double m_Scale = 1.0;

        private Vertex m_Translation = new Vertex() { X = 0.0, Y = 0.0 };

        private GeometryBuilder m_Geometry = new GeometryBuilder();

        private List<Vertex> m_A = new List<Vertex>();

        private List<Vertex> m_B = new List<Vertex>();

        private EditState m_Edit = EditState.None;

        private Action m_Append = null;

        private Operation m_Operation = Operation.Union;

        private void CalculateScaleAndTranslation()
        {
            bool init = false;
            var min = new Vertex() { X = 0.0, Y = 0.0 };
            var max = new Vertex() { X = 0.0, Y = 0.0 };
            for (int i = 0; i < m_Geometry.Vertexes.Count; i++)
            {
                var v = m_Geometry.Vertexes[i];
                if (init)
                {
                    min.X = Math.Min(v.X, min.X);
                    min.Y = Math.Min(v.Y, min.Y);
                    max.X = Math.Max(v.X, max.X);
                    max.Y = Math.Max(v.Y, max.Y);
                }
                else
                {
                    min = v;
                    max = v;
                    init = true;
                }
            }
            min.X -= 1.0f;
            min.Y -= 1.0f;
            max.X += 1.0f;
            max.Y += 1.0f;
            var width = max.X - min.X;
            var height = max.Y - min.Y;
            m_Scale = Math.Min(this.Width / width, this.Height / height);
            m_Translation = new Vertex() { X = (min.X + max.X) * 0.5f, Y = (min.Y + max.Y) * 0.5f };
        }

        private PointF ProjectPoint(Vertex vertex)
        {
            return new PointF((float)((vertex.X - m_Translation.X) * m_Scale) + this.Width * 0.5f, (float)((vertex.Y - m_Translation.Y) * m_Scale) + this.Height * 0.5f);
        }

        private Vertex UnprojectPoint(PointF point)
        {
            return new Vertex(((point.X - this.Width * 0.5f) / m_Scale) + m_Translation.X, ((point.Y - this.Height * 0.5f) / m_Scale) + m_Translation.Y);
        }

        public GeometryView()
        {
            this.DoubleBuffered = true;
        }

        private void PaintMouseMove(Graphics graphics, Point location, IList<Vertex> vertexes)
        {
            const int CROSS_SIZE = 20;
            graphics.DrawLine(Pens.White, new Point(location.X, location.Y - CROSS_SIZE), new Point(location.X, location.Y + CROSS_SIZE));
            graphics.DrawLine(Pens.White, new Point(location.X - CROSS_SIZE, location.Y), new Point(location.X + CROSS_SIZE, location.Y));
            if (vertexes.Count > 0)
            {
                graphics.DrawLine(Pens.White, ProjectPoint(vertexes[vertexes.Count - 1]), location);
                graphics.DrawLine(Pens.White, ProjectPoint(vertexes[0]), location);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            var graphics = e.Graphics;
            var a_count = m_A.Count;
            var b_count = m_B.Count;
            switch (m_Edit)
            {
                case EditState.None:
                    foreach (var triangle in m_Geometry.Triangles)
                    {
                        graphics.FillPolygon(Brushes.LightSteelBlue, new PointF[] { ProjectPoint(triangle.A), ProjectPoint(triangle.B), ProjectPoint(triangle.C), ProjectPoint(triangle.A) });
                    }
                    var format = new StringFormat() { Alignment = StringAlignment.Far, LineAlignment = StringAlignment.Far };
                    var height = this.FontHeight;
                    graphics.DrawString(String.Format("Площадь А - {0:f2}",  m_Geometry.AreaA), this.Font, Brushes.Red, this.Width - height, height * 2, format);
                    graphics.DrawString(String.Format("Площадь B - {0:f2}", m_Geometry.AreaB), this.Font, Brushes.Orange, this.Width - height, height * 4, format);
                    graphics.DrawString(String.Format("Площадь результата - {0:f2}", m_Geometry.AreaOperation), this.Font, Brushes.LightSteelBlue, this.Width - height, height * 6, format);
                    break;
                case EditState.A:
                    {
                        var location = PointToClient(Cursor.Position);
                        PaintMouseMove(e.Graphics, location, m_A);
                        a_count--;
                    }
                    break;
                case EditState.B:
                    {
                        var location = PointToClient(Cursor.Position);
                        PaintMouseMove(e.Graphics, location, m_B);
                        b_count--;
                    }
                    break;
                default:
                    break;
            }
            for (int i = 0; i < a_count; i++)
            {
                var v1 = ProjectPoint(m_A[i]);
                var v2 = ProjectPoint(m_A[(i + 1) % m_A.Count]);
                graphics.DrawLine(Pens.Red, v1, v2);
            }
            for (int i = 0; i < b_count; i++)
            {
                var v1 = ProjectPoint(m_B[i]);
                var v2 = ProjectPoint(m_B[(i + 1) % m_B.Count]);
                graphics.DrawLine(Pens.Orange, v1, v2);
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (e.Button == MouseButtons.Right)
            {
                if (m_Append != null)
                {
                    m_Append();
                }
                m_Append = null;
                m_Edit = EditState.None;
                UpdateView();
            }
            else
            {
                if (m_Edit == EditState.A)
                {
                    m_A.Add(UnprojectPoint(e.Location));
                    Invalidate();
                }
                else if (m_Edit == EditState.B)
                {
                    m_B.Add(UnprojectPoint(e.Location));
                    Invalidate();
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (m_Edit != EditState.None)
            {
                Invalidate();
            }
        }

        public IList<Vertex> A
        {
            get
            {
                return m_A;
            }
        }

        public IList<Vertex> B
        {
            get
            {
                return m_B;
            }
        }

        public Operation Operation
        {
            get
            {
                return m_Operation;
            }
            set
            {
                m_Operation = value;
            }
        }

        public void UpdateView()
        {
            m_Geometry.Build(m_A, m_B, m_Operation);
            CalculateScaleAndTranslation();
            Invalidate();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            CalculateScaleAndTranslation();
            Invalidate();
        }

        public void AppendA(Action action)
        {
            m_A.Clear();
            m_Edit = EditState.A;
            m_Append = action;
            Invalidate();
        }

        public void AppendB(Action action)
        {
            m_B.Clear();
            m_Edit = EditState.B;
            m_Append = action;
            Invalidate();
        }
    }
}
