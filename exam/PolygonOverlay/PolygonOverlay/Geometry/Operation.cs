﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonOverlay.Geometry
{
    enum Operation
    {
        Cross,
        Union,
        Difference
    }
}
